import 'package:flutter/material.dart';
import './app/routes/index.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'CHAOS',
      routes: routes,
      theme:
          ThemeData(primaryColor: Color(0xffFFFFFF), fontFamily: 'fangzheng'));
}
