import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'Request.dart';

class Data {

  Future<bool> add(domain, Map param) async {
    return await Request().post('/api/$domain/add', param: param);
  }

  Future<bool> remove(domain, String mu) async {
    return await Request().post('/api/$domain/delete', param: {'mu': mu});
  }

  Future<bool> update(domain, String mu, {Map? param}) async {
    return await Request()
        .post('/api/$domain/update', param: {'mu': mu, 'data': param});
  }

  Future<dynamic> one(domain, String mu) async {
    dynamic res = await Request().post('/api/$domain/one', param: {'mu': mu});
    if (res is bool) {
      return {};
    }
    return res;
  }

  Future<List<dynamic>> list(domain, {Map? param}) async {
    dynamic res = await Request().post('/api/$domain/list', param: param);
    if (res is bool) {
      return [];
    }
    return res;
  }

  Future<List<dynamic>> page(domain,
      {Map? param, int? pageNum = 1, int? pageSize = 15}) async {
    dynamic res = await Request().post('/api/$domain/page',
        param: {'pageNum': pageNum, 'pageSize': pageSize, 'data': param});
    if (res is bool) {
      return [];
    }
    return res;
  }

  Future<dynamic> query(url, {Map? param}) async {
    return await Request().post('/api/$url', param: param);
  }

  Future<List<dynamic>> search(url, {Map? param}) async {
    dynamic res = await Request().post('/api/$url', param: param);
    if (res is bool) {
      return [];
    }
    return res;
  }

  Future<bool> submit(url, {Map? param}) async {
    return await Request().post('/api/$url', param: param);
  }

  dynamic futureBuilding(BuildContext context, AsyncSnapshot snapshot,
      Widget widget) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.hasError) {
        return Text("Error: ${snapshot.error}");
      } else {
        return widget;
      }
    } else {
      return Text("Loding...");
    }
  }

}
