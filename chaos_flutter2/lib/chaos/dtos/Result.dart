import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class Result<T> {
  final int code;
  final String msg;
  final T? data;

  Result(this.code, this.msg, {this.data});

  Result.fromJson(Map<String, dynamic> json)
      : code = json['code'],
        msg = json['msg'],
        data = json['data'];

  Map<String, dynamic> toJson() => <String, dynamic>{
        'code': code,
        'msg': msg,
        'data': data,
      };
}
