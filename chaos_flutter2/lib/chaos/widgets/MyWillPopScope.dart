import 'package:flutter/material.dart';

class MyWillPopScope extends StatefulWidget {
  @override
  MyWillPopScopeState createState() {
    return new MyWillPopScopeState();
  }
}

class MyWillPopScopeState extends State<MyWillPopScope> {
  late DateTime _lastPressedAt;

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: () async {
          if (DateTime.now().difference(_lastPressedAt) >
              Duration(seconds: 1)) {
            _lastPressedAt = DateTime.now();
            return false;
          }
          return true;
        },
        child: Container(
          alignment: Alignment.center,
          child: Text("1秒内连续按两次返回键退出"),
        ));
  }
}
