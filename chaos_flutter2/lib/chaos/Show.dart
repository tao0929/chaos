import 'package:flutter/material.dart';

class Show {
  Future<bool?> showNormalDialog(context, Widget widget)async {
    return await showDialog<bool>(context: context, builder: (context) => widget);
  }

  Widget deleteAlert(context) {
    return AlertDialog(
      title: Text("提示"),
      content: Text("您确定要执行删除吗?"),
      actions: <Widget>[
        FlatButton(
          child: Text("取消"),
          onPressed: () => Navigator.of(context).pop(),
        ),
        FlatButton(
            child: Text("删除"),
            onPressed: () => Navigator.of(context).pop(true)),
      ],
    );
  }
}
