import 'package:dio/dio.dart';
import './../app/config.dart';
import 'dtos/Result.dart';

class Request {
  factory Request() => instance;

  Dio? get dio => _dio;
  Dio? _dio;

  static final Request instance = Request._internal();

  Request._internal() {
    if (_dio == null) {
      _dio = new Dio(BaseOptions(
          baseUrl: baseUrl,
          connectTimeout: 10000,
          receiveTimeout: 3000,
          headers: {
            'client': 'Android',
            'token': '123123',
            'version': '1.9.1',
          }));
    }
    _dio!.interceptors.add(LogInterceptor(responseBody: true));
  }

  /// 定义请求场景
  /// 1,加载数据,页面打开,上下拉刷新
  /// 1.1 Detail型数据,需要默认数据{}
  /// 1.2 Table型数据,需要[]
  /// 2.页面操作,提交数据 true/false,msg(可选)
  ///接口处理过程
  ///1.statusCode!=200 全局toast提示
  ///1.1 list/page型接口返回[]
  ///1.2 add/update型接口返回false
  ///1.3 one型接口返回null,页面需自定义默认值,或定义页面渲染逻辑流程
  Future post(String url, {dynamic param}) async {
    Response response = await dio!.post(url, data: param == null ? {} : param);
    if (response.statusCode != 200) {
      //toast();
      return;
    }
    Result result = Result.fromJson(response.data);
    if (result.code == 200) {
      return result.data;
    }
    //toast();
    return false;
  }
}
