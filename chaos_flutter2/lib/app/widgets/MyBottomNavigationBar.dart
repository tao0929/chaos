import 'package:chaos_flutter/app/consts/ImageUrlString.dart';
import 'package:chaos_flutter/app/consts/LabelString.dart';
import 'package:chaos_flutter/app/notifications/TabChangeMyNotification.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class MyBottomNavigationBar extends HookWidget  {
  List _imageUrls = [
    {
      'imageUrl': ImageUrl.bottomImage,
      'activeImageUrl': ImageUrl.bottomImageChecked
    },
    {
      'imageUrl': ImageUrl.bottomImage,
      'activeImageUrl': ImageUrl.bottomImageChecked
    },
    {
      'imageUrl': ImageUrl.bottomImage,
      'activeImageUrl': ImageUrl.bottomImageChecked
    },
    {
      'imageUrl': ImageUrl.bottomImage,
      'activeImageUrl': ImageUrl.bottomImageChecked
    },
  ];

  @override
  Widget build(BuildContext context) {
    final activeTabIndex = useState(0);

    buildIcon(index) => Image.asset(
        index == activeTabIndex
            ? _imageUrls[index]['activeImageUrl']
            : _imageUrls[index]['imageUrl'],
        width: 25.0,
        height: 25.0);

    List<BottomNavigationBarItem> _bars = [
      BottomNavigationBarItem(icon: buildIcon(0), label: LabelString.infos),
      BottomNavigationBarItem(icon: buildIcon(1), label: LabelString.mine),
    ];

    return BottomNavigationBar(
        items: _bars.map((item) => item).toList(),
        currentIndex: activeTabIndex.value,
        onTap: (index) {
          activeTabIndex.value = index;
          TabChangeMyNotification(index).dispatch(context);
        },
        backgroundColor: Color(0xfff6f9f8),
        type: BottomNavigationBarType.fixed,
        selectedItemColor: Color(0xff4F4F53),
        unselectedItemColor: Color(0xffB6B8B9));
  }
}
