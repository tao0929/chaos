import 'package:chaos_flutter/app/notifications/TabChangeMyNotification.dart';
import 'package:chaos_flutter/app/pages/home/tabs/infos/index.dart';
import 'package:chaos_flutter/app/pages/home/tabs/mine/index.dart';
import 'package:chaos_flutter/app/widgets/MyBottomNavigationBar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class HomePage extends HookWidget {
  @override
  Widget build(BuildContext context) {
    List<Widget> _tabs = [InfosTab(), MineTab()];

    final tabIndex = useState(0);

    buildBottomNavigationBar() => NotificationListener<TabChangeMyNotification>(
          onNotification: (notification) {
            tabIndex.value = notification.tabIndex;
            return true;
          },
          child: MyBottomNavigationBar(),
        );

    return Scaffold(
        body: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  colors: [Color(0xffFFFFFF), Color(0xffE8E7F6)],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter)),
          child: _tabs[tabIndex.value],
        ),
        bottomNavigationBar: buildBottomNavigationBar());
  }
}
