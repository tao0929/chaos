import 'package:chaos_flutter/app/consts/ImageUrlString.dart';
import 'package:chaos_flutter/app/dtos/ChaosInfo.dart';
import 'package:chaos_flutter/chaos/Data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class InfosTab extends HookWidget {
  @override
  Widget build(BuildContext context) {
    final listData = useState([]);

    load() async {
      listData.value = ChaosInfo.toList(await Data().list('chaosInfo'));
    }

    useEffect(() {
      load();
    }, []);

    buildHead() => Padding(
          padding: EdgeInsets.fromLTRB(16, 42.5, 21.5, 20.5),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [Text("头部")]),
        );

    buildInfos() => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: listData.value.length > 0
            ? listData.value.map((item) => Text(item['title'])).toList()
            : [Text('暂无数据')]);

    return SingleChildScrollView(
        child: Column(
      children: [buildHead(), buildInfos()],
    ));
  }
}
