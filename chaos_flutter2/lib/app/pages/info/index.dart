import 'package:chaos_flutter/app/dtos/ChaosInfo.dart';
import 'package:chaos_flutter/chaos/Data.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class InfoPage extends HookWidget {
  final String mu;

  InfoPage({required this.mu});

  @override
  Widget build(BuildContext context) => SingleChildScrollView(
          child: Column(
        children: [buildInfo()],
      ));

  buildInfo() => Center(
        child: FutureBuilder<String>(
          future: LoadData(),
          builder: (BuildContext context, AsyncSnapshot snapshot) => Data()
              .futureBuilding(
                  context, snapshot, Text("Contents: ${snapshot.data}")),
        ),
      );

  Future<String> LoadData() async =>
      ChaosInfo.fromJson(await Data().one('chaosInfo', '1')).title;
}
