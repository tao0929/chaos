import 'package:chaos_flutter/chaos/dtos/Dto.dart';
import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ChaosInfo extends Dto {
  late String title;
  late String info;
  late String url;

  ChaosInfo(this.title, this.info, this.url);

  ChaosInfo.fromJson(Map<String, dynamic> json)
      : title = json['title'],
        info = json['info'],
        url = json['url'];

  Map<String, dynamic> toJson() => <String, dynamic>{
        'title': title,
        'info': info,
        'url': url,
      };

  static List<ChaosInfo> toList(List<dynamic> listData) =>
      listData.map((e) => ChaosInfo.fromJson(e)).toList();
}
