import 'package:flutter/material.dart';

class TabChangeMyNotification extends Notification {
  TabChangeMyNotification(this.tabIndex);

  final int tabIndex;
}
