import 'package:chaos_flutter/app/pages/info/index.dart';
import 'package:flutter/material.dart';
import '../pages/home/index.dart';

final routes = {
  '/': (context) => HomePage(),
  'info': (context) =>
      InfoPage(mu:  ModalRoute.of(context)!.settings.arguments.toString())

};
