import { submit } from '@/chaos/functions/Data';
import { get, login, navigateTo } from '@/chaos/functions/Uni';

export async function doLogin({ referrer = '', source = '' }, force = false) {
    if (!force && get('token')) {
        return;
    }
    const { mu, token } = await submit('wxmini/login', { code: await login(), referrer, source });
    uni.setStorageSync('mu', mu);
    uni.setStorageSync('token', token);
}

export function checkPhone() {
    return new Promise((reslove) => {
        if (get('phone').match(/^1\d{10}/)) {
            reslove(true);
        } else {
            navigateTo('authorize');
        }
    });
}

export async function doLoad(props) {
    await doLogin(props);
    await checkPhone();
}
