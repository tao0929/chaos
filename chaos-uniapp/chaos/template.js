/**
 * pages模版快速生成脚本,执行命令 npm run tep `文件名`
 */

const fs = require('fs');

const dirName = process.argv[2];

if (!dirName) {
    console.log('文件夹名称不能为空！');
    console.log('示例：npm run tep test');
    process.exit(0);
}

// 页面模版
const indexTep = `
<template>
    <view class="page">
        火猩
    </view>
</template>

<script>
    import { list } from '@/chaos/functions/Data';
    import { getParams, path, get } from '@/chaos/functions/Uni';
    import { doLogin } from '@/app/utils/login';

    export default {
        data() {
            return {
                loading: true,
                list: [],

            }
        },
        async created(props = {}) {
            this.list = await list("chaosInfo");
            this.loading = false;
            doLogin(props, true);
        },
        methods: {

        }
    }
</script>

<style lang="stylus" scoped>

</style>
`;

fs.mkdirSync(`./src/pages/${dirName}`); // mkdir $1
process.chdir(`./src/pages/${dirName}`); // cd $1

fs.writeFileSync('index.vue', indexTep);

console.log(`模版${dirName}已创建,请手动增加models`);

function titleCase(str) {
    const array = str.toLowerCase().split(' ');
    for (let i = 0; i < array.length; i++) {
        array[i] = array[i][0].toUpperCase() + array[i].substring(1, array[i].length);
    }
    const string = array.join(' ');
    return string;
}

process.exit(0);
