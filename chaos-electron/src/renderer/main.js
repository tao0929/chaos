import Vue from 'vue';
import axios from 'axios';

import App from './chaos/App';
import router from './chaos/router';
import store from './chaos/vuex';

if (!process.env.IS_WEB) Vue.use(require('vue-electron'));
Vue.http = Vue.prototype.$http = axios;
Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
    components: { App },
    router,
    store,
    template: '<App/>',
}).$mount('#app');
