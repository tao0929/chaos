import Vue from 'vue';
import Router from 'vue-router';
import { routers } from '@/app/config';

Vue.use(Router);

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
    return originalPush.call(this, location).catch((err) => err);
};
let children = [
    {
        path: '/welcome',
        name: '欢迎',
        component: require('@/chaos/views/welcome/index').default,
    },
    {
        path: '/onlineAdmin',
        name: '在线管理员',
        component: require('@/chaos/views/admin/onlineAdmin').default,
    },
    {
        path: '/limit',
        name: '访问限制',
        component: require('@/chaos/views/admin/limit').default,
    },
    {
        path: '/onlineUser',
        name: '在线用户',
        component: require('@/chaos/views/admin/onlineUser').default,
    },
];
let routes = [
    {
        path: '*',
        redirect: '/',
    },
    {
        path: '/',
        name: '登录',
        component: require('@/app/views/login/index').default,
    },
    {
        path: '/main',
        redirect: '/welcome',
        component: require('@/chaos/layout/main/index').default,
        children: children.concat(routers()),
    },
];

export default new Router({ routes });
