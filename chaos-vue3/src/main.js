import { createApp } from 'vue';
import App from '@/chaos/App';
import router from '@/chaos/router/index';
import store from '@/chaos/vuex/index';
import VueWechatTitle from 'vue-wechat-title';
import ElementPlus from 'element-plus';

import '@/chaos/styles/reset.less';
import 'element-plus/lib/theme-chalk/index.css';
import '@/chaos/styles/theme.less';
import Avatar from '@/chaos/components/Avatar';
import PlainButton from '@/chaos/components/PlainButton';
import PrimaryButton from '@/chaos/components/PrimaryButton';

import SearchButton from '@/chaos/components/search/Button';
import SearchDatePicker from '@/chaos/components/search/DatePicker';
import SearchInput from '@/chaos/components/search/Input';
import SearchPagination from '@/chaos/components/search/Pagination';
import SearchRadioGroup from '@/chaos/components/search/RadioGroup';
import SearchSelect from '@/chaos/components/search/Select';
import mitt from 'mitt';

const app = createApp(App);

app.component('Avatar', Avatar);
app.component('PlainButton', PlainButton);
app.component('PrimaryButton', PrimaryButton);

app.component('SearchButton', SearchButton);
app.component('SearchDatePicker', SearchDatePicker);
app.component('SearchInput', SearchInput);
app.component('SearchPagination', SearchPagination);
app.component('SearchRadioGroup', SearchRadioGroup);
app.component('SearchSelect', SearchSelect);

app.config.productionTip = false;
app.config.globalProperties.$bus = new mitt();
app.use(ElementPlus);
app.use(router);
app.use(store);
app.use(VueWechatTitle);
app.mount('#app');
