const state = {
    user: {
        token: '',
        userMu: '',
        username: '',
        indexLink: '',
        roleInfo: '',
        roleName: ''
    },
    menus: []
};

const mutations = {
    SetUserinfo(
        state,
        { token, mu: userMu, username, indexLink, roleInfo, roleName }
    ) {
        state.user = { token, userMu, username, indexLink, roleInfo, roleName };
    },
    RefreshToken(state, token) {
        state.user.token = token;
    },
    Logout(state) {
        state.user = {
            token: '',
            userMu: '',
            username: '',
            indexLink: '',
            roleInfo: '',
            roleName: ''
        };
        state.menus = [];
    },
    SetMenus(state, data) {
        state.menus = data;
    }
};

const actions = {
    setUserinfo({ commit }, data) {
        commit('SetUserinfo', data);
    },
    refreshToken({ commit }, data) {
        commit('RefreshToken', data);
    },
    logout({ commit }) {
        commit('Logout');
    },
    setMenus({ commit }, data) {
        commit('SetMenus', data);
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    actions
};
