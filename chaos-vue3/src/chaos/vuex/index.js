import { createStore } from 'vuex';
import createVuexAlong from 'vuex-along';
import getters from './getters';
import global from './modules/global';
import admin from './modules/admin';
import app from './modules/app';

export default createStore({
    modules: {
        global,
        admin,
        app
    },
    getters,
    plugins: [createVuexAlong()]
});
