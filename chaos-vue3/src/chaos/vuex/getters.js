const getters = {
    user: ({ admin }) => admin.user,
    menus: ({ admin }) => admin.menus,
    lastPost: ({ app }) => app.lastPost,
    baseUrl: ({ app }) => app.baseUrl,
    isCollapse: ({ app }) => app.isCollapse,
    asideWidth: ({ app }) => app.asideWidth,
    data: ({ global }) => global.data
};
export default getters;
