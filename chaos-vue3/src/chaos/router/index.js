import { createRouter, createWebHashHistory } from 'vue-router';
import { routers } from '@/app/config';

let children = [
    {
        path: '/welcome',
        meta: {
            title: '欢迎'
        },
        component: () => import('@/chaos/views/welcome/index')
    },
    {
        path: '/onlineUser',
        name: 'onlineUser',
        meta: { title: '在线用户', keepAlive: true },
        component: () => import('@/chaos/views/admin/onlineUser')
    },
    {
        path: '/onlineAdmin',
        name: 'onlineAdmin',
        meta: { title: '在线管理员', keepAlive: true },
        component: () => import('@/chaos/views/admin/onlineAdmin')
    },
    {
        path: '/limit',
        name: 'limit',
        meta: { title: '访问限制' },
        component: () => import('@/chaos/views/admin/limit')
    }
];
let routes = [
    {
        path: '/',
        component: () => import('@/app/views/login/index'),
        meta: {
            title: '登录'
        }
    },
    {
        path: '/main',
        component: () => import('@/chaos/layout/main/index'),
        redirect: '/welcome',
        children: children.concat(routers())
    },
    {
        path: '/:pathMatch(.*)',
        redirect: '/'
    }
];

const router = createRouter({
    history: createWebHashHistory(),
    routes
});

export default router;
