import { ElMessage } from 'element-plus';

export function success(text) {
    ElMessage({
        type: 'success',
        message: text
    });
}

export function info(text) {
    ElMessage({
        type: 'info',
        message: text
    });
}

export function warning(text) {
    ElMessage({
        type: 'warning',
        message: text
    });
}

export default {
    info,
    success,
    warning
};
