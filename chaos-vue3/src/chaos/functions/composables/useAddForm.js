import { ref, reactive } from 'vue';
import { add } from '@/chaos/functions/common/Data';

export default function useAddForm(domain, search) {
    const showAddForm = ref(false);
    const formRef = ref(null);
    const form = reactive({});

    const showAdd = () => {
        showAddForm.value = true;
    };
    const doAdd = () => {
        formRef.value.validate(async valid => {
            if (!valid) {
                return false;
            }
            const res = await add(domain, form);
            if (res) {
                showAddForm.value = false;
                search();
            }
        });
    };
    return { showAddForm, form, formRef, showAdd, doAdd };
}
