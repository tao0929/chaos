export default {
    pages: ['app/pages/index/index', 'app/pages/webview/index'],
    window: {
        backgroundTextStyle: 'light',
        navigationBarBackgroundColor: '#fff',
        navigationBarTitleText: '火猩',
        navigationBarTextStyle: 'black'
    },
    permission: {
        'scope.userLocation': {
            desc: '获取位置'
        }
    }
};
