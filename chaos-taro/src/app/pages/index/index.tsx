import React, { useState, useEffect } from 'react';
import { View } from '@tarojs/components';
import { list } from '@/chaos/functions/Data';
import { getParams } from '@/chaos/functions/Taro';
import { doLogin } from '@/app/utils/login';
import styles from './index.module.scss';

const Index: React.FC = () => {
    const [listData, setListData] = useState<[]>();

    useEffect(() => {
        const load = async () => {
            setListData(await list('chaosInfo'));
            await doLogin(getParams(), true);
        };
        load();
    }, []);
    const goWebview = mu => {
        console.log('hello my dear', mu);
    };

    return (
        <View className={styles.page}>
            <View>Hello My Dear!</View>
            {listData &&
                listData.map(({ mu, title }: any, index) => (
                    <View className={styles.list_item} key={index} onClick={() => goWebview(mu)}>
                        {title}
                    </View>
                ))}
        </View>
    );
};

export default Index;
