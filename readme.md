![](https://img.shields.io/badge/language-java-483D8B.svg)
![](https://img.shields.io/badge/language-javascript-483D8B.svg)
![](https://img.shields.io/badge/language-TypeScript-483D8B.svg)
![](https://img.shields.io/badge/language-Dart-483D8B.svg)
![](https://img.shields.io/badge/license-MIT-483D8B.svg)

# CHAOS(混乱架构) 🍑
Chaos 致力于构建普适于研发领域的技术体系，研发体系，知识体系。         
Chaos 持续更新，使用热门技术，先进理念，迭代架构，更多特性敬请期待！       
​    

Chaos 是一个基于分布式、前后端分离设计的，持续集成、持续迭代，开箱即用的快速开发架构。      
Chaos 架构提供了服务端，前端，客户端，跨端的技术选型方案（优先使用业界最新解决方案），并通过统一的系统设计将各技术栈串联整合。         
Chaos 架构选型的语言包括且不限于 Java，Python，Go，Js/Ts，Dart/Kotlin。   
Chaos 架构选型的框架包括且不限于 SpringCloud，React/VUE，Taro/uniapp，ReactNative/Flutter。      
Chaos 架构约束的设计包括且不限于数据库表设计，API 设计，交互设计。      
Chaos 架构提供的框架整合支持包括 SpringCloudAlibaba，Vue3，Ant Design Pro，Taro，ReactNative，Flutter2。   

# [DaJue(聊聊技术)](https://ape-stack.gitee.io/dajue/)🍑		
# [热门技术](https://ape-stack.gitee.io/dajue/2.%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B/3.%E5%BF%AB%E9%80%9F%E5%BC%80%E5%A7%8B.html)	

## 架构组成

| 端     | 架构                         | 框架                          | 版本          | 状态 |
| ------ | ---------------------------- | ----------------------------- | ------------- | ---- |
| 服务端 | **chaos-SpringCloudAlibaba** | 基于 springcloud 的服务端架构 | **4.0.0**     | ✔    |
|        |                              | SpringBoot            | 2.5.8 |      |
|        |                              | SpringCloud            | 2020.0.4 |      |
|        |                              | SpringCloudAlibaba            | 2021.1 |      |
|        |                              | openFiegn                     |               |      |
|        |                              | dubbo                         | 2.7.8         |      |
|        |                              | nacos                         | 1.4.1         |      |
|        |                              | MybatisPlus                   | 3.4.0         |      |
|        |                              |                               |               |      |
| 前端   | **chaos-vue3**️              | 基于 vue 的中后台架构         | **1.0.0**     | ✔    |
|        |                              | vue                           | 3.0.11        |      |
|        |                              | vue-router                    | 4.0.6         |      |
|        |                              | vuex                          | 4.0.0         |      |
|        |                              | element-plus                  | 1.0.2-beta.44 |      |
|        |                              | axios                         | 0.21.1        |      |
|        | **chaos-ant.design.pro**     | 基于 react 的中后台架构       | **1.0.0**     | ✔    |
|        |                              | react                         | 17.0.0        |      |
|        |                              | umi                           | 3.4.0         |      |
|        |                              | antd                          | 4.14.0        |      |
|        |                              |                               |               |      |
| 小程序 | **chaos-weapp**              | 基于 wx-native 的小程序架构   | **1.0.0**     | ✔    |
|        |                              | vant                          | 1.6.8         |      |
|        | **chaos-taro**               | 基于 taro 的小程序架构        | **1.0.0**     | ✔    |
|        |                              | react                         | 17.0.0        |      |
|        |                              | taro                          | 3.2.6         |      |
|        |                              | taro-ui                       | 3.0.0-alpha.3 |      |
|        | **chaos-uniapp**             | 基于 uniapp 的小程序架构      | **1.0.0**     | ✔    |
|        |                              | uniapp-ui                     |               |      |
|        |                              |                               |               |      |
| 跨端   | **chaos_rn**                 | 基于 ReactNative 的跨端架构   |               |      |
|        | **chaos_flutter**            | 基于 flutter 的跨端架构       |               |      |
|        |                              | flutter                       | 2.2.3         |      |
|        |                              |                               |               |      |
| 桌面端 | **chaos-electron**           |                               |               |      |
|        |                              |                               |               |      |
|        |                              |                               |               |      |

## 最佳实践

全栈项目的最佳实践路径！

| 后端                     | 后台       | 小程序     | 跨端          | PC           |
| ------------------------ | ---------- | ---------- | ------------- | ------------ |
| chaos-SpringCloudAlibaba | chaos-vue3 | chaos-taro | chaos_flutter | chaos-nextjs |


# Poweredby 暴れる（Jacky）
