export default {
    dev: {
        '/manage/': {
            target: 'http://localhost:38899',
            changeOrigin: true,
            pathRewrite: { '^': '' },
        },
    },
};
