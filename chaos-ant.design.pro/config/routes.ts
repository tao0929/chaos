﻿export default [
    { path: '/', layout: false, component: '@/app/pages/user/login' },
    { path: '/welcome', name: 'welcome', component: '@/chaos/pages/welcome' },
    {
        path: '/onlineAdmin',
        name: 'onlineAdmin',
        access: 'canAdmin',
        component: '@/chaos/pages/onlineAdmin',
    },
    {
        path: '/onlineUser',
        name: 'onlineUser',
        access: 'canAdmin',
        component: '@/chaos/pages/onlineUser',
    },
    {
        path: '/limit',
        name: 'limit',
        access: 'canAdmin',
        component: '@/chaos/pages/limit',
    },
    {
        path: '/user',
        name: 'user',
        access: 'canAdmin',
        component: '@/app/pages/user',
    },
];
