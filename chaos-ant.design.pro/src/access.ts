export default function access(initialState: { currentUser: DTO.CurrentUser }) {
    const { currentUser } = initialState || {};
    return {
        // eslint-disable-next-line @typescript-eslint/no-unused-vars
        canAdmin: ({ path }: any) => {
            if (!currentUser || !currentUser.roleName) return false;
            return currentUser.roleName === 'ADMIN';
        },
    };
}
