import React from 'react';
import { Card, Typography } from 'antd';
import { PageContainer } from '@ant-design/pro-layout';

export default (): React.ReactNode => {
    return (
        <PageContainer>
            <Card>
                <Typography.Text strong>用户列表</Typography.Text>
            </Card>
        </PageContainer>
    );
};
