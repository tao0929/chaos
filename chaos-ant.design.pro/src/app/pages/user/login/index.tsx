import React, { useState } from 'react';
import { LockOutlined, MobileOutlined, UserOutlined } from '@ant-design/icons';
import { message, Tabs } from 'antd';
import { Link, useModel, history } from 'umi';
import ProForm, { ProFormCaptcha, ProFormText } from '@ant-design/pro-form';
import Footer from '@/chaos/components/Footer';
import { submit } from '@/chaos/functions/Data';

import styles from './index.less';

const Login: React.FC = () => {
    const [submitting, setSubmitting] = useState(false);
    const [type, setType] = useState<string>('account');
    const { setInitialState } = useModel<any>('@@initialState');

    const handleSubmit = async (data: any) => {
        setSubmitting(true);
        const currentUser = await submit('login', data);
        setSubmitting(false);
        if (!currentUser) {
            message.success('登录失败！');
            return;
        }
        setInitialState({ currentUser });
        localStorage.setItem('token', currentUser.token);
        message.success('登录成功！');
        history.push(currentUser.indexLink);
    };

    const getCaptcha = async (phone: any) => {
        return phone == '18888888888';
    };

    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <div className={styles.top}>
                    <div className={styles.header}>
                        <Link to="/">
                            <img alt="logo" className={styles.logo} src="./logo.png" />
                            <span className={styles.title}>CHAOS</span>
                        </Link>
                    </div>
                    <div className={styles.desc}>快速开发架构</div>
                </div>
                <div className={styles.main}>
                    <ProForm
                        submitter={{
                            searchConfig: { submitText: '登录' },
                            render: (_, dom) => dom.pop(),
                            submitButtonProps: {
                                loading: submitting,
                                size: 'large',
                                style: { width: '100%' },
                            },
                        }}
                        onFinish={async (value) => handleSubmit(value)}
                    >
                        <Tabs activeKey={type} onChange={setType}>
                            <Tabs.TabPane key="account" tab="账户密码登录" />
                            <Tabs.TabPane key="mobile" tab="手机号登录" />
                        </Tabs>
                        {type === 'account' && (
                            <>
                                <ProFormText
                                    name="username"
                                    fieldProps={{
                                        size: 'large',
                                        prefix: <UserOutlined className={styles.prefixIcon} />,
                                    }}
                                    placeholder="请输入用户名"
                                    rules={[{ required: true, message: '请输入用户名' }]}
                                />
                                <ProFormText.Password
                                    name="password"
                                    fieldProps={{
                                        size: 'large',
                                        prefix: <LockOutlined className={styles.prefixIcon} />,
                                    }}
                                    placeholder="请输入密码"
                                    rules={[{ required: true, message: '请输入密码！' }]}
                                />
                            </>
                        )}
                        {type === 'mobile' && (
                            <>
                                <ProFormText
                                    fieldProps={{
                                        size: 'large',
                                        prefix: <MobileOutlined className={styles.prefixIcon} />,
                                    }}
                                    name="mobile"
                                    placeholder="手机号"
                                    rules={[
                                        { required: true, message: '请输入手机号！' },
                                        { pattern: /^1\d{10}$/, message: '手机号格式错误！' },
                                    ]}
                                />
                                <ProFormCaptcha
                                    fieldProps={{
                                        size: 'large',
                                        prefix: <LockOutlined className={styles.prefixIcon} />,
                                    }}
                                    captchaProps={{ size: 'large' }}
                                    placeholder="请输入验证码"
                                    captchaTextRender={(timing, count) => {
                                        if (timing) {
                                            return `${count} 获取验证码`;
                                        }
                                        return '获取验证码';
                                    }}
                                    name="captcha"
                                    rules={[{ required: true, message: '请输入验证码！' }]}
                                    onGetCaptcha={async (phone) => {
                                        const result = await getCaptcha({ phone });
                                        if (result === false) {
                                            return;
                                        }
                                        message.success('获取验证码成功！');
                                    }}
                                />
                            </>
                        )}
                        <div style={{ marginBottom: 24 }} />
                    </ProForm>
                </div>
            </div>
            <Footer />
        </div>
    );
};

export default Login;
