// @ts-ignore
/* eslint-disable */

declare namespace DTO {
    type CurrentUser = {
        indexLink: string;
        mu: string;
        phone: string;
        roleInfo: string;
        roleName: string;
        token: string;
        username: string;
        menus: [];
    };

    type PageParams = {
        current?: number;
        pageSize?: number;
    };
}
