import type { MenuDataItem, Settings as LayoutSettings } from '@ant-design/pro-layout';
import type { RequestConfig, RunTimeLayoutConfig } from 'umi';
import RightContent from '@/chaos/components/RightContent';
import Footer from '@/chaos/components/Footer';
import { PageLoading } from '@ant-design/pro-layout';
import { history } from 'umi';
import { errorHandler, requestInterceptor, responseInterceptor } from '@/chaos/functions/Request';

export const request: RequestConfig = {
    errorHandler,
    requestInterceptors: [requestInterceptor],
    responseInterceptors: [responseInterceptor],
};

export const initialStateConfig = { loading: <PageLoading /> };

export async function getInitialState(): Promise<{
    settings?: Partial<LayoutSettings>;
    currentUser?: any;
}> {
    return { settings: {} };
}

export const layout: RunTimeLayoutConfig = ({ initialState }) => {
    return {
        rightContentRender: () => <RightContent />,
        footerRender: () => <Footer />,
        menuDataRender: () => {
            if (!initialState?.currentUser) {
                return [];
            }
            const menus: MenuDataItem[] | { name: any; routes: any[] }[] = [];
            initialState?.currentUser.menus.forEach(({ title: name = '', submenus = [] }) => {
                const routes: { path: any; name: any }[] = [];
                submenus.forEach(({ title: subname, link }) => {
                    routes.push({ path: link, name: subname });
                });
                menus.push({ name, routes });
            });
            return menus;
        },
        disableContentMargin: false,
        waterMarkProps: {
            content: initialState?.currentUser?.mu,
        },
        onPageChange: () => {
            const { location } = history;
            if (!initialState?.currentUser && location.pathname !== '/') {
                history.push('/');
            }
        },
        menuHeaderRender: undefined,
        ...initialState?.settings,
    };
};
