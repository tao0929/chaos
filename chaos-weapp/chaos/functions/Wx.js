export async function login() {
    return new Promise((reslove, reject) => {
        wx.login({ success: (res) => reslove(res.code), fail: () => reject() });
    });
}

export function userProfile() {
    return new Promise((reslove, reject) => {
        wx.getUserProfile({
            desc: '用于完善会员资料',
            success: (res) => reslove(res.userInfo),
            fail: () => reject(),
        });
    });
}

export function location() {
    return new Promise((reslove, reject) => {
        wx.getLocation({
            type: 'gcj02',
            success: (res) => reslove(res),
            fail: () => reject(),
        });
    });
}

export function get(key) {
    return wx.getStorageSync(key);
}

export function successToast(title) {
    wx.showToast({ title });
}

export function failToast(title) {
    wx.showToast({ icon: 'none', image: '/app/images/fail.png', title });
}

export function infoToast(title) {
    wx.showToast({ icon: 'none', title });
}

export function showLoading() {
    wx.showLoading({ title: '加载中' });
}

export function hideLoading() {
    wx.hideLoading();
}

export function navigateTo(url, params = '') {
    wx.navigateTo({ url: path(url) + (params && '?') + params });
}

export function navigateBack(delta = 1) {
    wx.navigateBack({ delta });
}

export function redirectTo(url, params = '') {
    wx.redirectTo({ url: path(url) + (params && '?') + params });
}

export function reLaunch(url) {
    wx.reLaunch({ url: path(url) });
}

export function path(url) {
    return `/app/pages/${url}/index`;
}

export function barTitle(title) {
    wx.setNavigationBarTitle({ title });
}

export function message() {
    return new Promise((reslove, reject) => {
        wx.requestSubscribeMessage({
            tmplIds: [''],
            success: (res) => reslove(res),
            fail: () => reject(),
        });
    });
}
