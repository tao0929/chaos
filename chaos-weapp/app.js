import { reLaunch } from '/chaos/functions/Wx';
const localHost = 'http://127.0.0.1:58899';
const devHost = 'http://127.0.0.1:58899';
const prodHost = 'http://127.0.0.1:58899';

App({
    onPageNotFound: () => reLaunch('index'),
    require: ($uri) => require($uri),
    host: () => localHost,
});
