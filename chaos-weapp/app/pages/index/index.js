const { navigateTo, path, get, showLoading, hideLoading } = getApp().require(
    '/chaos/functions/Wx'
);
const { list } = getApp().require('/chaos/functions/Data');
const { checkPhone, doLogin } = getApp().require('/app/utils/login');

Page({
    data: {
        loading: true,
        list: [],
    },
    async onLoad(props) {
        showLoading();
        this.setData({ list: await list('chaosInfo') });
        hideLoading();
        doLogin(props, true);
    },
    async goWebview(e) {
        navigateTo('webview', 'mu=' + e.currentTarget.dataset.mu);
    },
    onShareAppMessage() {
        return {
            title: '火猩科技',
            path: `${path('index')}?referrer=${get('mu')}&source=首页`,
        };
    },
});
