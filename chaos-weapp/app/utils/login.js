const { get, location, login, navigateTo } = getApp().require(
    '/chaos/functions/Wx'
);
const { submit, update } = getApp().require('/chaos/functions/Data');

export function checkPhone() {
    return new Promise((reslove, reject) => {
        if (get('phone').match(/^1\d{10}/)) {
            reslove(true);
        } else {
            navigateTo('authorize');
        }
    });
}

export async function doLogin({ referrer = '', source = '' }, force = false) {
    if (!force && get('token')) {
        return;
    }
    const { mu, token } = await submit('wxmini/login', {
        code: await login(),
        referrer,
        source,
    });
    wx.setStorageSync('mu', mu);
    wx.setStorageSync('token', token);
}
async function doLocation() {
    const { longitude: lon, latitude: lat } = await location();
    wx.setStorageSync('location', lon + ';' + lat);
    update('chaosUser', '', { lon, lat });
}

export async function doLoad(props) {
    await doLogin(props);
    await checkPhone();
}
