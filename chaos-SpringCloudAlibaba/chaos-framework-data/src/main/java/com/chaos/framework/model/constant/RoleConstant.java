package com.chaos.framework.model.constant;

/**
 * @author Jacky
 * @date 2020/11/5 21:42
 */
public interface RoleConstant {
    String GUEST = "GUEST";

    String ADMIN = "ADMIN";
    String DEV = "DEV";

    //运营
    String OP = "OP";
}
