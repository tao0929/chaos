package com.chaos.framework.model.config;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@EnableFeignClients(basePackages = "com.chaos")
@Configuration
public class OpenFeignConfig {
}
