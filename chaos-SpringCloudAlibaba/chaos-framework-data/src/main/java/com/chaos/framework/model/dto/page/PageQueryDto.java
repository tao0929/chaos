package com.chaos.framework.model.dto.page;

import com.chaos.framework.model.constant.Constants;
import com.chaos.framework.model.dto.DTO;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class PageQueryDto<T> extends DTO {
    private Integer pageNum = Constants.DEFAULT_PAGE;
    private Integer pageSize = Constants.DEFAULT_PAGE_SIZE;
    private T data;

}
