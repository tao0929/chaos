package com.chaos.framework.model.dto.result.data;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class MarkDataResult<T> extends DataResult {
    private String mark;

}
