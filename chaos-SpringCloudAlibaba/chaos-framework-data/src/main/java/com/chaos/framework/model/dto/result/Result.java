package com.chaos.framework.model.dto.result;

import com.chaos.framework.model.dto.DTO;
import lombok.*;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class Result extends DTO {

    private Integer code;

    private String msg;

    public void failure() {
        this.setCode(ResultEnum.FAILURE.getCode());
        this.setMsg(ResultEnum.FAILURE.getDefaultMsg());
    }

    public void unknow() {
        this.setCode(ResultEnum.UNKONW_ERROR.getCode());
        this.setMsg(ResultEnum.UNKONW_ERROR.getDefaultMsg());
    }

    public void success() {
        this.setCode(ResultEnum.SUCCESS.getCode());
        this.setMsg(ResultEnum.SUCCESS.getDefaultMsg());
    }

    public void msg(Integer code, String msg) {
        this.setCode(code);
        this.setMsg(msg);
    }

}
