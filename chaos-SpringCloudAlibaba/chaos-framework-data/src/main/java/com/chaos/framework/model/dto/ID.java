package com.chaos.framework.model.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

/**
 * @author Jacky
 * @date 2019/11/7 17:30
 */
@Data
@NoArgsConstructor
@RequiredArgsConstructor(staticName = "of")
public class ID extends DTO {
    @NonNull
    private Long id;

    public String idString() {
        return id.toString();
    }



}
