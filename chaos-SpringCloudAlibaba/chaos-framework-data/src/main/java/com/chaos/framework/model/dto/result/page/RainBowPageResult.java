package com.chaos.framework.model.dto.result.page;

import com.chaos.framework.model.dto.page.RainBowPageList;
import com.chaos.framework.model.dto.result.Result;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class RainBowPageResult<T> extends Result {
    private RainBowPageList<T> page;

}
