package com.chaos.framework.model.data;

import com.chaos.framework.model.entity.Table;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Jacky
 * @date 2020/2/26 15:57
 */
@Data
@NoArgsConstructor
public class OrderByData extends DATA {
    private String orderBy = Table.ID;
}
