package com.chaos.framework.model.dto.page;

import com.chaos.framework.model.dto.DTO;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class PageList<T> extends DTO {
    private List<T> list;
    private Long total;
    private Long current;

}
