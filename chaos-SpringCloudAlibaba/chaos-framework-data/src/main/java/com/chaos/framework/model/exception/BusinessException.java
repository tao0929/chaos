package com.chaos.framework.model.exception;

import com.chaos.framework.model.dto.result.Result;
import com.chaos.framework.model.dto.result.ResultEnum;
import lombok.Data;

@Data
public class BusinessException extends RuntimeException {
    private Result result;

    public BusinessException() {
        super(ResultEnum.FAILURE.getDefaultMsg());
        this.result = new Result(ResultEnum.FAILURE.getCode(), ResultEnum.FAILURE.getDefaultMsg());
    }

    public BusinessException(ResultEnum resultEnum) {
        super(resultEnum.getDefaultMsg());
        this.result = new Result(resultEnum.getCode(), resultEnum.getDefaultMsg());
    }

    public BusinessException(Integer code, String msg) {
        super(msg);
        this.result = new Result(code, msg);
    }

}
