package com.chaos.framework.model.dto.result.page;

import lombok.*;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class MarkPageResult<T> extends PageResult {

    private String mark;


}
