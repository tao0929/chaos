package com.chaos.framework.model.dto.result.list;

import com.chaos.framework.model.dto.result.Result;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

/**
 * @author Jacky
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class ListResult<T> extends Result {

    /**
     * 响应数据
     */
    private List<T> list;


}
