package com.chaos.framework.model.dto.page;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class RainBowPageList<T> extends PageList {
    private T top;
    private T down;

}
