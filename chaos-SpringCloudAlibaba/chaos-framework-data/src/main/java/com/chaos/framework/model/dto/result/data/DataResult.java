package com.chaos.framework.model.dto.result.data;

import com.chaos.framework.model.dto.result.Result;
import com.chaos.framework.model.dto.result.ResultEnum;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class DataResult<T> extends Result {


    private T data;

    private Integer code;

    public boolean isSuccess() {
        return ResultEnum.SUCCESS.getCode().intValue() == this.getCode().intValue() ? true : false;
    }

    public DataResult(String msg) {
        this.setMsg(msg);
    }

    public DataResult(ResultEnum re) {
        this.setCode(re.getCode());
        this.setMsg(re.getDefaultMsg());
    }

}
