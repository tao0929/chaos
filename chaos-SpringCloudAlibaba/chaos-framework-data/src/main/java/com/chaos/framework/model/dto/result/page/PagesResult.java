package com.chaos.framework.model.dto.result.page;

import com.chaos.framework.model.dto.page.PageList;
import com.chaos.framework.model.dto.result.Result;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.ArrayList;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class PagesResult<T> extends Result {

    private ArrayList<PageList<T>> pages;

}
