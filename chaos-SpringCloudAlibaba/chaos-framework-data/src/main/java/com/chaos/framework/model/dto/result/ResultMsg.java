package com.chaos.framework.model.dto.result;

import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 * @date 2020/10/13 15:13
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class ResultMsg {
    private String success;
    private String failure;
}
