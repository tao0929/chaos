package com.chaos.framework.model.dto.result.page;

import com.chaos.framework.model.dto.page.PageList;
import com.chaos.framework.model.dto.result.Result;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper =false)
@Accessors(chain = true)
public class PageResult<T> extends Result {
    private PageList<T> page;

}
