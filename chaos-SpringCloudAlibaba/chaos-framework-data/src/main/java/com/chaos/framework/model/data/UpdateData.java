package com.chaos.framework.model.data;


import lombok.*;
import lombok.experimental.Accessors;

/**
 * @author Jacky
 * @date 2020/2/26 15:56
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class UpdateData<T> extends DATA {
    private T data;

    public UpdateData(Long id, T data) {
        this.setId(id);
        this.data = data;
    }

    public static UpdateData of(Long id, Object data) {
        return new UpdateData(id, data);
    }
}
