package com.chaos.framework.model.dto;

import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author Jacky
 * @date 2019/11/7 17:30
 */
@NoArgsConstructor
public class DTO implements Serializable {
    private static final long serialVersionUID = 42L;

}
