package com.chaos.framework.model.data;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Jacky
 * @date 2020/2/26 15:57
 */
@Data
@NoArgsConstructor
public class TenantData extends DATA {
    private Integer tenantId;

}
