package com.chaos.framework.model.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.chaos.framework.model.dto.DTO;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Jacky
 * @date 2020/2/26 15:57
 */
@Data
@NoArgsConstructor
public class DATA extends DTO {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long id;

}
