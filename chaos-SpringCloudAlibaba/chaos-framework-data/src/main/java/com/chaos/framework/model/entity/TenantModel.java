package com.chaos.framework.model.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Jacky
 * @date 2020/5/26 11:58
 */
@Data
@NoArgsConstructor
public class TenantModel extends Model {

    @TableField("tenant_id")
    private Integer tenantId;

}
