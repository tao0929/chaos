package ${cfg.p}.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import PageHelper;
import ConvertService;
import UpdateData;
import ID;
import PageList;
import PageQueryDto;
import Table;
import ${cfg.p}.api.entity.${entity};
import ${cfg.p}.api.data.${entity}Data;
import ${cfg.p}.api.service.I${entity}Service;
import ${cfg.p}.service.mapper.${entity}Mapper;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;

import java.util.List;
/**
* <p>
* ${table.comment!} 服务实现类
* </p>
*
* @author ${author}
* @since ${date}
*/
@Slf4j
@Component
public class ${table.serviceImplName} extends ServiceImpl<${table.mapperName}, ${entity}> implements ${table.serviceName} {
	@Autowired
	private ConvertService convertService;

	@Override
	public ID insertModel(${entity}Data data) {
		${entity} entity = (${entity}) convertService.convertToModel(data, ${entity}.class);
		return save(entity) ? ID.of(entity.getId()) : null;
	}

	@Override
	public boolean deleteModel(ID data) {
		return removeById(data.getId());
	}

	@Override
	public boolean updateModelByID(UpdateData<${entity}Data> data) {
		${entity} entity = (${entity}) convertService.convertToModel(data.getData(), ${entity}.class);
		return update(entity, new UpdateWrapper<${entity}>().eq(Table.ID, data.getId()));
	}

	@Override
	public ${entity}Data selectByID(ID data) {
		${entity} entity = getOne(new QueryWrapper<${entity}>().eq(Table.ID, data.getId()));
		return (${entity}Data) convertService.convertToDTO(entity, ${entity}Data.class);
	}

	@Override
	public List<${entity}Data> selectByData(${entity}Data data) {
		QueryWrapper<${entity}> query = new QueryWrapper<${entity}>();
		query.lambda().eq(!StringUtils.isEmpty(data.getId()), ${entity}::getId, data.getId());
		query.orderByDesc(Table.ID);
		return convertService.convertToDTO(list(query), ${entity}Data.class);
	}

	@Override
	public PageList<${entity}Data> selectByPage(PageQueryDto<${entity}Data> pageData) {
		QueryWrapper<${entity}> query = new QueryWrapper<${entity}>();
		query.lambda().eq(!StringUtils.isEmpty(pageData.getData().getId()), ${entity}::getId, pageData.getData().getId());
		query.orderByDesc(Table.ID);
		IPage page = page(PageHelper.page(pageData), query);
		return PageHelper.pageList(page, ${entity}Data.class);
	}

}
