package ${cfg.p}.manage.controller;

import UpdateData;
import ${cfg.p}.api.data.${entity}Data;
import ${cfg.p}.api.service.I${entity}Service;
import ID;
import PageQueryDto;
import DataResult;
import PageResult;
import BaseController;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

import lombok.extern.slf4j.Slf4j;

/**
*
* @author ${author}
* @since ${date}
*/
@Slf4j
@RestController
@Api(tags = "${entity}MnController")
@RequestMapping("/manage/${table.entityPath}")
public class ${entity}MnController extends ${superControllerClass} {
    @Autowired
    private I${entity}Service i${entity}Service;

    @PostMapping("/add")
    @ApiOperation(value = "", httpMethod = "POST")
    public DataResult<ID> add(@RequestBody @Validated ${entity}Data data, BindingResult bindingResult) throws Exception {
        validate(bindingResult);
        return dataResult(i${entity}Service.insertModel(data));
    }

    @PostMapping("/update")
    @ApiOperation(value = "", httpMethod = "POST")
    public DataResult<Boolean> update( @ApiParam(value = "") @RequestBody @Validated UpdateData<${entity}Data> data, BindingResult bindingResult) throws Exception {
        validate(bindingResult);
        return dataResult(i${entity}Service.updateModelByID(data));
    }

    @PostMapping("/one")
    @ApiOperation(value = "", httpMethod = "POST")
    public DataResult<${entity}Data> one( @RequestBody ID data) throws Exception {
        return dataResult(i${entity}Service.selectByID(data));
    }

    @PostMapping("/list")
    @ApiOperation(value = "列表", httpMethod = "POST")
    public DataResult<List<${entity}Data>> list( @RequestBody ${entity}Data data) throws Exception {
        return dataResult(i${entity}Service.selectByData(data));
    }

    @PostMapping("/page")
    @ApiOperation(value = "", httpMethod = "POST")
    public PageResult<${entity}Data> page( @RequestBody PageQueryDto<${entity}Data> data) throws Exception {
        return pageResult(i${entity}Service.selectByPage(data));
    }

    @PostMapping("/delete")
    @ApiOperation(value = "删除", httpMethod = "POST")
    public DataResult<Boolean> delete( @RequestBody ID data) throws Exception {
        return dataResult(i${entity}Service.deleteModel(data));
    }

}
