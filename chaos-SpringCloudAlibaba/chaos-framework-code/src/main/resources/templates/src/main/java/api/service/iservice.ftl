package ${cfg.p}.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import ${cfg.p}.api.entity.${entity};
import ${cfg.p}.api.data.${entity}Data;
import UpdateData;
import ID;
import PageList;
import PageQueryDto;

import java.util.List;

/**
 *
 * @author ${author}
 * @since ${date}
*/
public interface ${table.serviceName} extends IService<${entity}> {

    ID insertModel(${entity}Data data);

    boolean deleteModel(ID data);

    boolean updateModelByID(UpdateData<${entity}Data> data);

    ${entity}Data selectByID(ID data);

    List<${entity}Data> selectByData(${entity}Data data);

    PageList<${entity}Data> selectByPage(PageQueryDto<${entity}Data> pageData);
}
