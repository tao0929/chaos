package com.chaos.framework.starter;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jacky
 */
@Configuration
@ConditionalOnWebApplication
@EnableConfigurationProperties(ChaosProperties.class)
public class ChaosServiceAutoConfiguration {

    @Autowired
    ChaosProperties chaosProperties;

    @Bean
    public ChaosService chaosService() {
        ChaosService service = new ChaosService();
        service.setChaosProperties(chaosProperties);
        return service;
    }


}