package com.chaos.framework.app.service;

import com.chaos.framework.model.entity.Model;
import com.chaos.framework.model.dto.DTO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * @author Jacky Cui
 * @date 2020/5/15 18:42
 */
@Component
public class ConvertService<T extends Model> {

    public List<DTO> convertToDTO(List<T> models, Class clazz) {
        return models.stream().map(model -> convertToDTO(model, clazz)).collect(Collectors.toList());
    }

    public List<DTO> convertToDTO(List<T> models, Class clazz, Consumer<DTO> consumer) {
        return models.stream().map(model -> convertToDTO(model, clazz, consumer)).collect(Collectors.toList());
    }

    public List<DTO> convertToDTO(List<T> models, Class clazz, BiConsumer<Model, DTO> consumer) {
        return models.stream().map(model -> convertToDTO(model, clazz, consumer)).collect(Collectors.toList());
    }

    public DTO convertToDTO(T model, Class clazz) {
        DTO target = null;
        if (model == null) {
            return null;
        }
        try {
            target = (DTO) clazz.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(model, target);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return target;
    }

    public DTO convertToDTO(T model, Class clazz, BiConsumer<Model, DTO> consumer) {
        DTO dto = convertToDTO(model, clazz);
        consumer.accept(model, dto);
        return dto;
    }

    public DTO convertToDTO(T model, Class clazz, Consumer<DTO> consumer) {
        DTO dto = convertToDTO(model, clazz);
        consumer.accept(dto);
        return dto;
    }


    public Model convertToModel(DTO dto, Class clazz) {
        Model model = null;
        if (dto == null) {
            return null;
        }
        try {
            model = (Model) clazz.getDeclaredConstructor().newInstance();
            BeanUtils.copyProperties(dto, model);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return model;

    }

    public List<Model> convertToModel(List<DTO> dtos, Class clazz) {
        return dtos.stream().map((dto) -> convertToModel(dto, clazz)).collect(Collectors.toList());
    }




}
