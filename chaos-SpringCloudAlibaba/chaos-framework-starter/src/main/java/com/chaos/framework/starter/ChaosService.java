package com.chaos.framework.starter;

public class ChaosService {

    ChaosProperties chaosProperties;

    public ChaosProperties getChaosProperties() {
        return chaosProperties;
    }

    public void setChaosProperties(ChaosProperties chaosProperties) {
        this.chaosProperties = chaosProperties;
    }

    public String sayHello(String name) {
        return chaosProperties.getPrefix() + "-" + name + chaosProperties.getSuffix();
    }
}
