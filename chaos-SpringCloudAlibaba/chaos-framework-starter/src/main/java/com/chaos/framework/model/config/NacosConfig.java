package com.chaos.framework.model.config;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

/**
 * @author Jacky
 * @date 2020/10/29 15:57
 */
@Configuration
@EnableDiscoveryClient
public class NacosConfig {
}
