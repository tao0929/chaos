package com.chaos.framework.web.base;


import com.google.common.collect.Lists;
import com.chaos.framework.model.dto.page.PageList;
import com.chaos.framework.model.dto.result.ResultEnum;
import com.chaos.framework.model.dto.result.ResultMsg;
import com.chaos.framework.model.dto.result.data.DataResult;
import com.chaos.framework.model.dto.result.page.MarkPageResult;
import com.chaos.framework.model.dto.result.page.MarkPagesResult;
import com.chaos.framework.model.dto.result.page.PageResult;
import com.chaos.framework.model.exception.BusinessException;
import com.chaos.framework.model.service.RedisService;
import com.chaos.framework.web.service.TenantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import javax.servlet.http.HttpServletRequest;
import java.util.stream.Collectors;

public abstract class BaseController<T> {
    @Autowired
    private RedisService redisService;
    @Autowired
    protected HttpServletRequest request;
    @Autowired
    private TenantService tenantService;

//    public Integer getAdminId() {
//        return request.getHeader("token");
//    }
//
//    public Integer getUserId() {
//        return request.getHeader("token");
//    }

    public int getTenantId() {
        return tenantService.getTenantId();
    }

    public void validate(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            String msg = String.join(",", bindingResult.getFieldErrors().stream().map(FieldError::getDefaultMessage).collect(Collectors.toList()));
            throw new BusinessException(ResultEnum.VALIDATE.getCode(), msg);
        }
    }

    public DataResult dataResult(ResultMsg msg) {
        return dataResult(null, msg);
    }

    public DataResult dataResult(T data) {
        return dataResult(data, null);
    }

    public DataResult dataResult(T data, ResultMsg msg) {
        DataResult dataResult = new DataResult();
        if (data == null || (data instanceof Boolean && (Boolean) data == false)) {
            if (msg == null || StringUtils.isEmpty(msg.getFailure())) {
                dataResult.failure();
            } else {
                dataResult.msg(ResultEnum.FAILURE.getCode(), msg.getFailure());
            }
            dataResult.setData(data);
        } else {
            if (msg == null || StringUtils.isEmpty(msg.getSuccess())) {
                dataResult.success();
            } else {
                dataResult.msg(ResultEnum.SUCCESS.getCode(), msg.getSuccess());
            }
            dataResult.setData(data);
        }
        return dataResult;
    }

    public PageResult<T> pageResult(PageList<T> pageList) {
        PageResult<T> listResult = new PageResult<T>();
        if (pageList == null) {
            listResult.failure();
            listResult.setPage(new PageList());
        } else {
            listResult.success();
            listResult.setPage(pageList);
        }
        return listResult;
    }

    public MarkPageResult<T> pageResult(PageList<T> pageList, String mark) {
        MarkPageResult<T> listResult = new MarkPageResult<T>();
        if (pageList == null) {
            listResult.failure();
            listResult.setPage(new PageList());
        } else {
            listResult.success();
            listResult.setPage(pageList);
        }
        listResult.setPage(pageList);
        listResult.setMark(mark);
        return listResult;
    }

    public MarkPagesResult<T> pageResult(PageList<T> pl1, PageList<T> pl2, String mark) {
        MarkPagesResult<T> mpr = new MarkPagesResult<T>();
        if (pl1 == null || pl2 == null) {
            mpr.failure();
            mpr.setPages(Lists.newArrayList(new PageList(), new PageList()));
        } else {
            mpr.success();
            mpr.setPages(Lists.newArrayList(pl1, pl2));
        }
        mpr.setMark(mark);
        return mpr;
    }

}
