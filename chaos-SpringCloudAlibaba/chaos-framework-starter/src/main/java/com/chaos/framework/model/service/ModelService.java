package com.chaos.framework.model.service;

import com.chaos.framework.model.data.DATA;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jacky
 * @date 2020/7/2 21:33
 */
@Component
public class ModelService<T extends DATA> {
    public List<Long> getIds(List<T> list) {
        return list.stream().map(DATA::getId).collect(Collectors.toList());
    }

}
