package com.chaos.framework.model.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author Jacky
 * @date 2019/11/9 23:06
 */
@Configuration
@EnableScheduling
public class SchedulingConfig {
}
