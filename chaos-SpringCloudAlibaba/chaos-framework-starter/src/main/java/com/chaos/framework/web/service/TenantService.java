package com.chaos.framework.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

@Component
public class TenantService {
    @Autowired
    protected HttpServletRequest request;

    public int getTenantId() {
        String s = request.getHeader("tenantId");
        if (s != null) {
            return Integer.parseInt(s);
        }
        Object o = request.getAttribute("tenantId");
        if (o != null) {
            return Integer.parseInt((String) o);
        }
        return -1;
    }

    /**
     * 当ignoreTenant=1,表示本次请求忽略租户隔离
     */
    public boolean isIgnoreTenant() {
        Integer ignoreTenant = request.getIntHeader("ignoreTenant");
        if (ignoreTenant != null && ignoreTenant == 1) {
            return true;
        }
        Object o = request.getAttribute("ignoreTenant");
        if (o != null && Integer.parseInt((String) o) == 1) {
            return true;
        }
        return false;


    }


}
