import { useRouter } from 'next/router';

const Info = () => {
    const router = useRouter();
    const { mu } = router.query;

    return <div>Info:{mu}</div>;
};

export default Info;
