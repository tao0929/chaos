import { list } from '../../chaos/functions/Data';

export const getStaticProps = async (context) => {
    const listData = await list('chaosInfo');
    if (!listData) {
        return {
            redirect: { destination: '/', permanent: false },
        };
    }
    return {
        props: { listData },
    };
};

const Infos = ({ listData }) => {
    return (
        <ul>
            {listData.length > 0 &&
                listData.map(({ title }, index) => (
                    <li key={index}>{title}</li>
                ))}
        </ul>
    );
};
export default Infos;
