import { host, noLog } from './../../config';
const noConsole = noLog();

export async function post(url, params) {
    if (!noConsole) {
        console.log(
            `${new Date().toLocaleString()}【 M=${url} 】P=${JSON.stringify(
                params
            )}`
        );
    }
    const res = await fetch(`${host()}/${url}`, {
        headers: { 'Content-Type': 'application/json' },
        method: 'post',
        body: JSON.stringify(params),
    });
    const resData = await res.json();
    const { status, message, code, msg, ...rest } = resData;
    if (status) {
        console.log(
            `${new Date().toLocaleString()}【 status=${status} 】【错误信息=${message}】`
        );
        return false;
    }
    if (code === 200) {
        if (!noConsole) {
            console.log(
                `${new Date().toLocaleString()}【 M=${url} 】【接口响应：】`,
                { ...rest }
            );
        }
        return { ...rest };
    } else {
        return false;
    }
}
