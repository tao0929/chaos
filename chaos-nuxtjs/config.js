export function baseUrl() {
  let baseUrl = ''
  switch (process.env.NODE_ENV) {
    case 'development':
      baseUrl = ''
      break
    case 'production':
      baseUrl = '/chaos-api'
      break
  }
  return baseUrl
}
