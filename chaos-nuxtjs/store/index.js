export const state = () => ({
  user: {
    token: '',
    userMu: '',
    username: '',
    indexLink: '',
    roleInfo: '',
    roleName: '',
  },
})

export const mutations = {
  SetUserinfo(state, data) {
    state.user.token = data.token
    state.user.userMu = data.mu
    state.user.username = data.username
    state.user.indexLink = data.indexLink
    state.user.roleInfo = data.roleInfo
    state.user.roleName = data.roleName
  },
  RefreshToken(state, token) {
    state.user.token = token
  },
  Logout(state) {
    state.user.token = ''
    state.user.userMu = ''
    state.user.username = ''
    state.user.indexLink = ''
    state.user.roleInfo = ''
    state.user.roleName = ''
    state.menus = []
  },
}

export const actions = {
  setUserinfo({ commit }, data) {
    commit('SetUserinfo', data)
  },
  refreshToken({ commit }, data) {
    commit('RefreshToken', data)
  },
  logout({ commit }) {
    commit('Logout')
  },
}

export const getters = {
  user: (state) => state.admin.user,
}
