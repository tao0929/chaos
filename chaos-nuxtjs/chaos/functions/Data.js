import request from './Request'
export async function list(domain, params) {
    const { data = [] } = await request.post(`manage/${domain}/list`, {})
    return data
}
