import axios from 'axios'
import { baseUrl } from '@/config'

axios.defaults.headers.post['Content-Type'] = 'application/json'
const request = axios.create({
    baseURL: baseUrl(),
    timeout: 30000,
})

request.interceptors.request.use(
    (config) => {
        return config
    },
    (error) => {
        Promise.reject(error)
    }
)

request.interceptors.response.use(
    ({ status, data, config }) => {
        if (status !== 200) {
            return false
        }
        const { code, msg, ...rest } = data
        if (code === 200) {
            return { ...rest }
        }
        if (code === 201) {
            return false
        }
        if (code === 401 || code === 403) {
            return false
        }
        if (code === 408) {
            return false
        }
        if (code === 500) {
            return false
        }
        if (code === 501) {
            return false
        }
        if (code === 560) {
            return false
        }
        if (code === 555) {
            return false
        }
    },
    (error) => {
        if (error && error.response && error.response.status) {
            switch (error.response.status) {
                case 400:
                    error.message = '错误请求'
                    break
                case 401:
                    error.message = '未授权，请重新登录'
                    break
                case 403:
                    error.message = '拒绝访问'
                    break
                case 404:
                    error.message = '请求错误,未找到该资源'
                    break
                case 405:
                    error.message = '请求方法未允许'
                    break
                case 408:
                    error.message = '请求超时'
                    break
                case 500:
                    error.message = '服务器端出错'
                    break
                case 501:
                    error.message = '网络未实现'
                    break
                case 502:
                    error.message = '网络错误'
                    break
                case 503:
                    error.message = '服务不可用'
                    break
                case 504:
                    error.message = '网络超时'
                    break
                case 505:
                    error.message = 'http版本不支持该请求'
                    break
                default:
                    error.message = `未知错误${error.response.status}`
            }
        } else {
            error.message = '服务器未响应'
        }
        return false
    }
)

export default request
