import 'react-native-gesture-handler';
import {AppRegistry} from 'react-native';
import {name} from './app.json';
import AppContainer from './app/router';

AppRegistry.registerComponent(name, () => AppContainer);
