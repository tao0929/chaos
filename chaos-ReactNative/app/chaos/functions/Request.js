import {host, noLog} from './../../config';
const noConsole = noLog();
export const post = async (method, params) => {
  const url = `${host()}/${method}`;
  if (!noConsole) {
    console.log(
      `${new Date().toLocaleString()}【 M=${url} 】P=${JSON.stringify(params)}`,
    );
  }
  try {
    const res = await fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(params),
    });
    let resData = await res.json();

    //   if (statusCode != 200) {
    //     throw new Error(`网络请求错误，状态码${statusCode}`);
    //   }
    if (!noConsole) {
      console.log(
        `${new Date().toLocaleString()}【 M=${url} 】【接口响应：】`,
        resData,
      );
    }
    //   const {msg, code, ...rest} = res;
    //   if (code === 200) {
    //     return {...rest};
    //   }
    //   if (code === 201) {
    //     return post(url, params);
    //   }
    //   if (code === 401) {
    //     await doLogin({}, true);
    //     return post(url, params);
    //   }
    //   if (code === 412) {
    //     navigateTo('authorize');
    //   }
  } catch (error) {
    console.error(error);
  }
};
