import React, {useCallback} from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator, Easing, Animated} from 'react-navigation-stack';

import IndexScreen from './pages/index/index';
import UserScreen from './pages/user/index';
import LoginScreen from './pages/login/index';

const AppNavigator = createStackNavigator(
  {
    Index: {screen: IndexScreen},
    User: {screen: UserScreen},
  },
  {
    initialRouteName: 'Index',
    mode: 'modal',
    headerMode: 'screen',
    headerLayoutPreset: 'center',
    cardOverlayEnabled: true,
    transitionConfig: () => ({
      transitionSpec: {
        duration: 300,
        easing: Easing.out(Easing.poly(4)),
        timing: Animated.timing,
      },
      screenInterpolator: sceneProps => {
        const {layout, position, scene} = sceneProps;
        const {index} = scene;

        const height = layout.initHeight;
        const translateY = position.interpolate({
          inputRange: [index - 1, index, index + 1],
          outputRange: [height, 0, 0],
        });
        const opacity = position.interpolate({
          inputRange: [index - 1, index - 0.99, index],
          outputRange: [0, 1, 1],
        });
        return {opacity, transform: [{translateY}]};
      },
    }),
    defaultNavigationOptions: {
      title: '火猩',
    },
  },
);

const LoginNavigator = createStackNavigator(
  {
    Login: {screen: LoginScreen},
  },
  {mode: 'modal'},
);

const AppContainer = createAppContainer(
  createSwitchNavigator(
    {
      AppNavigator,
      LoginNavigator,
    },
    {
      initialRouteName: 'AppNavigator',
    },
  ),
);

export default () => {
  const onNavigationStateChange = useCallback((prevState, newState, action) => {
    console.log(prevState, newState, action);
  }, []);

  return <AppContainer onNavigationStateChange={onNavigationStateChange} />;
};
