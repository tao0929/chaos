/* eslint-disable react-hooks/exhaustive-deps */
import React, {useCallback, useEffect} from 'react';
import {View, Text, Button} from 'react-native';
import {list} from './../../chaos/functions/Data';

import styles from './index.style';

const Index = ({navigation}) => {
  const navigationToUser = useCallback(() => {
    navigation.navigate('User');
  }, []);
  const navigationToLogin = useCallback(() => {
    navigation.navigate('Login');
  }, []);
  useEffect(() => {
    const load = async () => {
      const listData = await list('chaosInfo');
      console.log(listData);
    };
    load();
  }, []);
  return (
    <View style={styles.flexContainer}>
      <Text style={styles.flexText}>Hello World</Text>
      <Button onPress={navigationToUser} title="go to user" />
      <Button onPress={navigationToLogin} title="go to login" />
    </View>
  );
};

Index.navigationOptions = {
  title: '首页',
};

export default Index;
