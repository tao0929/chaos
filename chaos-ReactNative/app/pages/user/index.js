import React from 'react';

import {View, Text} from 'react-native';

import styles from './index.style';

export default () => (
  <View style={styles.flexContainer}>
    <Text style={styles.flexText}>User</Text>
  </View>
);
