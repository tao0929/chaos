/* eslint-disable react-hooks/exhaustive-deps */
import React, {useCallback} from 'react';
import {View, Text, Button} from 'react-native';

import styles from './index.style';

export default ({navigation}) => {
  const navigationToIndex = useCallback(() => {
    navigation.navigate('Index');
  }, []);
  return (
    <View style={styles.flexContainer}>
      <Text style={styles.flexText}>Index</Text>
      <Button onPress={navigationToIndex} title="go to index" />
    </View>
  );
};
